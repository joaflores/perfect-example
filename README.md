# The Perfect Example Task

***Description***

The system consists of 2 services: flight-status and event-listener, as well as ElasticSearch and Kibana. Each of the services run on a Docker container.

**flight-status.** This REST API allows to search the status of a flight. When receives a request this API generates an event and send it to Azure Event Hubs.

Request parameters: 
* Flight number
* Travel date

Output:
* Flight number
* Flight route
* Departure time
* Arrival time
* Status

**event-listener.** This service listen for new events in Azure Event Hubs and pull them to send them to ElasticSearch.


***Technical specifications***

Both API are coded in Java using Spring boot, H2 in memory database for storing sample of flight data (used in flight-status) and libraries to connect with Azure Event Hubs and Elasticsearch (event-listener). 
Each API run in a separate Docker container.
The preconfigured Docker images and the source code is available in the Gitlab project: https://gitlab.com/joaflores/perfect-example


**Deployment instructions**

Clone the repository: 

`$ git clone https://gitlab.com/joaflores/perfect-example.git`

Load the included Docker images:

```
$ docker load < flight-status-svc_latest.tar.gz
$ docker load < event-listener-svc_latest.tar.gz 
$ docker load < elastic-kibana_latest.tar.gz 
```


Create a network:

`$ docker network create --subnet=172.20.0.0/16 perfectnet`

Run the containers:
```
$ docker run -p 9200:9200 -p 5601:5601 --net perfectnet --ip 172.20.0.2 --hostname elastik01 --name elastik01 -d nshou/elasticsearch-kibana

$ docker run -p 8080:8080 --net perfectnet --ip 172.20.0.3 --hostname flight-status01 --name flight-status01 -d flight-status-svc

$ docker run -p 8081:8081 --net perfectnet --ip 172.20.0.4 --hostname event-listener01 --name event-listener01 -d event-listener-svc
```


Check all the containers are running:

`$ docker ps -a`

**Usage**

When all the services are up and running open the web browser and go to http://localhost:8080/swagger-ui.html to see the operations available in the flight-status API. Try the “/flight/status/” operation to query the sample data. With every request an Event is generated and thus feeding ElasticSearch.
