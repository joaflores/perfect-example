package com.perfect.kafka.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.perfect.kafka.config.ApplicationConfig;
import com.perfect.kafka.model.EventEntity;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class EventService {

    @Autowired
    ApplicationConfig appConfig;

    private static final Logger LOGGER = LoggerFactory.getLogger(EventService.class);

    public void sendToElastic(String message){

        EventEntity event = stringToEvent(message);
        String uri = appConfig.url+ "/perfect/_doc";

        RestTemplate restTemplate = new RestTemplate();

        LOGGER.info(uri);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<String>(eventToJson(event), headers);

        String resultAsJsonStr = restTemplate.postForObject(uri, request, String.class);

        LOGGER.info(resultAsJsonStr);


    }

    private String eventToJson(EventEntity event){
        String json=null;
        ObjectMapper objMapp = new ObjectMapper();
        try {
            json = objMapp.writeValueAsString(event);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    private EventEntity stringToEvent(String message){
        EventEntity event=null;
        ObjectMapper objMapp = new ObjectMapper();

        try {
            event = objMapp.readValue(message,EventEntity.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            event=null;
        }

        return event;
    }
}
