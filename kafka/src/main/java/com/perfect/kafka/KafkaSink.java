package com.perfect.kafka;

import com.perfect.kafka.service.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@EnableBinding(Sink.class)
public class KafkaSink {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaSink.class);

    @Autowired
    EventService eventSvc;

    @StreamListener(Sink.INPUT)
    public void handleMessage(String message)  {

        //receives a message from Event Hub and it sends to Elasticsearch
        LOGGER.info("New message received: " + message);

        eventSvc.sendToElastic(message);

    }

}
