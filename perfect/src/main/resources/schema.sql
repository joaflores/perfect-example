DROP TABLE IF EXISTS FLIGHT;

CREATE TABLE FLIGHT (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  flight_number VARCHAR(6) NOT NULL,
  flight_route VARCHAR(9) NULL,
  departure_date DATE NULL,
  departure_time TIMESTAMP NULL,
  arrival_time TIMESTAMP NULL,
  status VARCHAR(7) NULL
);