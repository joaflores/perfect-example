package com.example.perfect.controller;

import com.example.perfect.model.Event;
import com.example.perfect.model.Flight;
import com.example.perfect.service.EventHubService;
import com.example.perfect.service.FlightService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.GenericMessage;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/flight")
@Api(value="flight")
public class FlightController {
    private Logger logger= LogManager.getLogger(FlightController.class);

    @Autowired
    FlightService flightSrv;

    @Autowired
    EventHubService eventHubSrv;



    @RequestMapping(path = "/status", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getStatus(@RequestParam("flight_number") String flight_number,
                                       @RequestParam("travel_date")
                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate travel_date ){

        logger.info("New request received...");
        String eventMessage="Status request for flight "+ flight_number + " with travel date " + travel_date;

        //send event info to Azure Event Hub
        eventHubSrv.sendMsgToEventHub(eventMessage, LocalDateTime.now());

        HttpStatus httpStatus = HttpStatus.OK;
        Flight flightResponse = flightSrv.getFlight(flight_number,travel_date);

        if (flightResponse==null){
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(flightResponse, httpStatus);
    }

    @GetMapping("/test")
    public String test(){
        return "Hello from perfect example app";
    }
}
