package com.example.perfect.service;

import com.example.perfect.model.Flight;
import com.example.perfect.repository.FlightRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class FlightService {

    private Logger logger= LogManager.getLogger(FlightService.class);

    @Autowired
    FlightRepository repository;

    public Flight getFlight(String flightNumber, LocalDate flightDate){

        Flight flight = null;

        if (flightNumber!=null && !flightNumber.isEmpty() && flightDate!=null){
            flight=repository.findFlightByNumberAndDate(flightNumber,flightDate);
        }

        return flight;
    }
}
