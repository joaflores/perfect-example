package com.example.perfect.service;

import com.example.perfect.model.Event;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


@EnableBinding(Source.class)
@Service
public class EventHubService {

    @Autowired
    private Source source;

    private Logger logger= LogManager.getLogger(EventHubService.class);

    public void sendMsgToEventHub(String message, LocalDateTime dateTime){


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        String eventDateTime = dateTime.format(formatter);

        Event event = new Event("event", "flight-status", eventDateTime,message);
        ObjectMapper objectMapper = new ObjectMapper();

        //Send event as json to Event hub
        try {
            this.source.output().send(new GenericMessage<>(objectMapper.writeValueAsString(event)));
        } catch (JsonProcessingException e) {
            logger.error("Error parsing event message. " + e.getMessage());
        }

    }
}
