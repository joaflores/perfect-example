package com.example.perfect.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(
            notes = "The flight number",
            example = "1130",
            position = 1)
    @Column(name="flight_number")
    private String flight_number;

    @ApiModelProperty(
            notes = "The flight route",
            example = "DUS-BRU",
            position = 2)
    @Column(name="flight_route")
    private String flight_route;

    @ApiModelProperty(
            notes = "The departure date",
            example = "2020-01-25",
            position = 3)
    @Column(name="departure_date")
    private LocalDate departure_date;

    @ApiModelProperty(
            notes = "The departure time",
            example = "2020-01-25T14:30",
            position = 4)
    @Column(name="departure_time")
    private LocalDateTime departure_time;

    @ApiModelProperty(
            notes = "The arrival time",
            example = "2020-01-25T15:30",
            position = 5)
    @Column(name="arrival_time")
    private LocalDateTime arrival_time;

    @ApiModelProperty(
            notes = "Status of the flight",
            example = "On Time",
            position = 6)
    @Column(name="status")
    private String status;

    /*Getters and setters*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFlight_number() {
        return flight_number;
    }

    public void setFlight_number(String flight_number) {
        this.flight_number = flight_number;
    }

    public String getFlight_route() {
        return flight_route;
    }

    public void setFlight_route(String flight_route) {
        this.flight_route = flight_route;
    }

    public LocalDate getDeparture_date() {
        return departure_date;
    }

    public void setDeparture_date(LocalDate departure_date) {
        this.departure_date = departure_date;
    }

    public LocalDateTime getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(LocalDateTime departure_time) {
        this.departure_time = departure_time;
    }

    public LocalDateTime getArrival_time() {
        return arrival_time;
    }

    public void setArrival_time(LocalDateTime arrival_time) {
        this.arrival_time = arrival_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
