package com.example.perfect.model;

public class Event {
    private String type;
    private String app;
    private String eventDate;
    private String msg;

    public Event(){}

    public Event(String type,String app,String eventDate,String msg){
        this.app=app;
        this.type=type;
        this.eventDate=eventDate;
        this.msg=msg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
