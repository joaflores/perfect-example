package com.example.perfect.repository;

import com.example.perfect.model.Flight;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;

@Repository
public class FlightRepositoryImpl implements FlightRepositoryExt {

    private Logger logger= LogManager.getLogger(FlightRepositoryImpl.class);
    private EntityManager em;

    public FlightRepositoryImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Flight findFlightByNumberAndDate(String flightNumber, LocalDate flightDate){

        Flight flightFound = null;

        logger.info("Getting into repository...");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Flight> cq = cb.createQuery(Flight.class);

        Root<Flight> flight = cq.from(Flight.class);

        Predicate hasFlightDate = cb.equal(flight.get("departure_date"),flightDate.atStartOfDay().toLocalDate());

        Predicate hasNumber = cb.equal(flight.get("flight_number"),flightNumber);

        cq.where(cb.and(hasNumber, hasFlightDate));

        try{
            flightFound = em.createQuery(cq.select(flight)).getSingleResult();
            logger.info("found");
        }
        catch (Exception e){
            logger.error(e.getMessage());
        }

        return flightFound;
    }

}
