package com.example.perfect.repository;

import com.example.perfect.model.Flight;

import java.time.LocalDate;

public interface FlightRepositoryExt {
    Flight findFlightByNumberAndDate(String flightNumber, LocalDate flightDate);
}
