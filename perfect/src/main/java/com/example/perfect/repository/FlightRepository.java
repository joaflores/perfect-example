package com.example.perfect.repository;

import com.example.perfect.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightRepository
        extends JpaRepository<Flight, Long>, FlightRepositoryExt,JpaSpecificationExecutor<Flight> {
}
